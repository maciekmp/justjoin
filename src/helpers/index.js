export const corsFetch = (url) => {
  const corsServices = [
    'https://cors.io/?',
    'https://cors-anywhere.herokuapp.com/',
  ];
  const randomServiceId = Math.floor(Math.random() * (corsServices.length));
  const randomService = corsServices[randomServiceId];
  try {
    return fetch(`${randomService}${url}`);
  } catch (e) {
    return corsFetch(url);
  }
}