import React, { Component } from 'react';
import ReactMapboxGL, { Popup, Layer } from "react-mapbox-gl";
import { withRouter } from 'react-router-dom';
import mbxGeocoding from "@mapbox/mapbox-sdk/services/geocoding"
import './style.less';

const accessToken = "pk.eyJ1IjoibWFjaWVrbXAiLCJhIjoiY2prbzVxcWZrMTRwcDNzdGhpdXZ4bTI3aiJ9.Jwgft0ZNf9quXGe-JVYfWA";

const geocodingClient = mbxGeocoding({
  accessToken,
});

const Map = ReactMapboxGL({
  accessToken,
});

const paintLayer = {
  'fill-extrusion-color': '#aaa',
  'fill-extrusion-height': {
    type: 'identity',
    property: 'height'
  },
  'fill-extrusion-base': {
    type: 'identity',
    property: 'min_height'
  },
  'fill-extrusion-opacity': 0.6
};

class MapBox extends Component {
  constructor() {
    super();

    this.state = {
      bbox: [
        [13.31743943, 49.002026002146465],
        [24.940675555533772, 54.93574600188205],
      ],
      clustering: false,
      layer3d: false,
      layerRaster: false,
      mapKey: 0,
      details: null,
    };
  }

  componentDidMount() {
    // can be replaced by country of user
    this.setCenterByName('Poland');
  }

  componentDidUpdate(prevProps, prevState) {
    // if (!prevProps.offerId && this.props.offerId) {
    //   const offer = this.props.offers.find((offer) => offer.id === this.props.offerId);
    //   this.map && this.map.flyTo({
    //     center: [+offer.longitude, +offer.latitude],
    //     zoom: 16,
    //     pitch: 60,
    //   });
    // }
    if (prevState.layerRaster !== this.state.layerRaster) {
      // this.updateMap();
    }
  }

  setCenterByName(name) {
    return geocodingClient.forwardGeocode({
      query: name,
      limit: 1,
    })
      .send()
      .then((response) => {
        if (response.body.features.length === 0) {
          return false;
        }
        const { bbox } = response.body.features[0];
        return this.setState({
          bbox: [
            [bbox[0], bbox[1]],
            [bbox[2], bbox[3]],
          ],
        });
      });
  }

  getOfferById(offerId) {
    return this.props.offers.find((offer) => offer.id === offerId);
  }

  getFeatures() {
    return this.props.offers
      .map((offer) => ({
        id: offer.id,
        "type": "Feature",
        "properties": {
          marker_icon: offer.marker_icon,
        },
        "geometry": {
          "type": "Point",
          "coordinates": [+offer.longitude, +offer.latitude],
        }
      }));
  }

  getMapStyle() {
    return this.state.layerRaster ? {
      "version": 8,
      "sources": {
        "raster-tiles": {
          "type": "raster",
          "url": "mapbox://mapbox.dark",
          "tileSize": 256
        }
      },
      "layers": [{
        "id": "simple-tiles",
        "type": "raster",
        "source": "raster-tiles",
        "minzoom": 0,
        "maxzoom": 22,
        before: 'offers',
      }]
    } :
      'mapbox://styles/mapbox/dark-v9';
  }

  loadIcons() {
    const imagesArray = [
      'ruby',
      'python',
      'php',
      'c',
      'mobile',
      'net',
      'html',
      'javascript',
      'java',
      'scala',
      'other',
      'testing',
      'devops',
      'ux',
      'pm',
      'game'
    ];
    imagesArray.forEach(image => {
      const url = `/icons/${image}.png`;
      this.map.loadImage(url, (error, imageData) => {
        this.map.addImage(image, imageData);
      });
    });
  }

  addOffersLayer() {
    const features = this.getFeatures();
    this.map.addSource('offers', {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features,
      },
      cluster: this.state.clustering,
      clusterRadius: 40
    });

    this.map.addLayer({
      id: "clusters",
      type: "circle",
      source: "offers",
      filter: ["has", "point_count"],
      paint: {
        "circle-color": [
          "step",
          ["get", "point_count"],
          "#51bbd6",
          100,
          "#f1f075",
          200,
          "#f28cb1"
        ],
        "circle-radius": [
          "step",
          ["get", "point_count"],
          20,
          100,
          30,
          200,
          40
        ]
      }
    });
    this.map.addLayer({
      id: "cluster-count",
      type: "symbol",
      source: "offers",
      filter: ["has", "point_count"],
      layout: {
        "text-field": "{point_count_abbreviated}",
        "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
        "text-size": 12
      }
    });
    this.map.addLayer({
      id: 'offers',
      type: 'symbol',
      source: 'offers',
      filter: ["!", ["has", "point_count"]],
      layout: {
        'icon-image': '{marker_icon}',
        'icon-allow-overlap': true,
        'icon-size': 0.5,
      }
    });
    this.map.on('click', 'clusters', (e) => {
      var features = this.map.queryRenderedFeatures(e.point, { layers: ['clusters'] });
      var clusterId = features[0].properties.cluster_id;
      this.map.getSource('offers').getClusterExpansionZoom(clusterId, (err, zoom) => {
        if (err)
          return;

        this.map.easeTo({
          center: features[0].geometry.coordinates,
          zoom: zoom
        });
      });
    });

    this.map.on('mouseenter', 'clusters', () => {
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', 'clusters', () => {
      this.map.getCanvas().style.cursor = '';
    });
  }

  updateMap = () => {
    if (!this.map) {
      return false;
    }
  }

  updateDetails() {
    this.map &&
      this.setState({
        details: {
          zoom: this.map.getZoom(),
          bounds: this.map.getBounds(),
          center: this.map.getCenter(),
        }
      })
  }

  render() {
    const { popup, popupOffers } = this.state;
    return (
      <Map
        key={this.state.mapKey}
        style={this.getMapStyle()}
        containerStyle={{
          height: "100%",
          width: "100%",
        }}
        fitBounds={this.state.bbox}
        onStyleLoad={(map) => {
          this.map = map;
          this.loadIcons();
          this.addOffersLayer();
          this.updateMap();
          this.updateDetails();
        }}
        onDragEnd={() => {
          this.updateDetails();
        }}
        onZoomEnd={() => {
          this.updateDetails();
        }}
      >
        <div className="map-options">
          <label>
            <input
              type="checkbox"
              checked={this.state.clustering}
              onChange={() => {
                this.setState({
                  clustering: !this.state.clustering,
                  mapKey: this.state.mapKey + 1,
                });
              }}
            />
            clustering
          </label>
          <label>
            <input
              type="checkbox"
              checked={this.state.layer3d}
              onChange={() => {
                this.setState({
                  layer3d: !this.state.layer3d,
                });
              }}
            />
            layer 3d
          </label>
          <label>
            <input
              type="checkbox"
              checked={this.state.layerRaster}
              onChange={() => {
                this.setState({
                  layerRaster: !this.state.layerRaster,
                  mapKey: this.state.mapKey + 1,
                });
              }}
            />
            layer raster
          </label>
          {this.state.details &&
            Object.keys(this.state.details).map(key => (
              <div
                key={key}
              >
                {key}: {this.state.details[key].toString()}
              </div>
            ))}
        </div>
        {popup &&
          <Popup
            coordinates={popup}
          >
            {popupOffers.map(offer => (
              <div
                key={offer.id}
              >
                <img src={offer.company_logo_url} alt={offer.company_name} />
                <h3>
                  {offer.title}
                </h3>
                <div>
                  {offer.salary_from}-{offer.salary_to} {offer.salary_currency}
                </div>
                {offer.company_name}
              </div>
            ))}
          </Popup>
        }
        {this.state.layer3d &&
          <Layer
            id="3d-buildings"
            sourceId="composite"
            sourceLayer="building"
            filter={['==', 'extrude', 'true']}
            type="fill-extrusion"
            minZoom={14}
            paint={paintLayer}
          />
        }
      </Map>
    );
  }
}

export default withRouter(MapBox);