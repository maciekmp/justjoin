import React from 'react';
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';

import MapBox from './components/mapbox';
import './App.less';

class App extends React.Component {
  state = {
    loading: true,
    offers: [],
    cities: [],
    markers: [],
  }
  fetchOffers() {
    const url = 'https://cors-anywhere.herokuapp.com/https://justjoin.it/api/offers';
    // const url = 'https://s3-eu-west-1.amazonaws.com/offers.justjoin.it/1500offers.json';
    fetch(url)
      .then(response => response.json())
      .then(offers => {
        const citiesObject = {};
        const markersObject = {};

        offers.forEach(offer => {
          const city = offer.city;
          citiesObject[city] = citiesObject[city] ? ++citiesObject[city] : 1;
          const marker = offer.marker_icon;
          markersObject[marker] = markersObject[marker] ? ++markersObject[marker] : 1;
        });
        const cities = Object.entries(citiesObject)
          .sort((a, b) => b[1] - a[1])
          .slice(0, 10);

        const markers = Object.entries(markersObject)
          .sort((a, b) => b[1] - a[1])
          .slice(0, 15);

        this.setState({
          loading: false,
          offers,
          cities,
          markers,
        });
      });
  }
  componentDidMount() {
    this.fetchOffers();
  }
  renderOffer(offer) {
    return (
      <li
        key={offer.id}
      >
        {offer.title ?
          <Link
            to={'/offers/' + offer.id}
            className={offer.marker_icon}
          >
            <div className="border" />
            <div className="image-container">
              <img
                src={offer.company_logo_url}
                alt={offer.id}
              />
            </div>
            <div className="name-container">
              <h3>
                {offer.title}
              </h3>
              <span>
                {offer.company_name}
              </span>
              <span>
                {offer.address_text}
              </span>
            </div>
            <div className="right-container">
              <span className="price">
                {offer.salary_from ?
                  <span>
                    {offer.salary_from} - {offer.salary_to} {offer.salary_currency.toUpperCase()}
                  </span> :
                  ' - '}
              </span>
              <span className="badge">
                6d ago
  </span>
              <div />
              {offer.skills.map(skill => (
                <span
                  key={`${offer.id}${skill.name}`}
                  className="badge"
                >
                  {skill.name}
                </span>
              ))}
            </div>
          </Link>
          : null}
      </li>
    );
  }
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <div className="header">
            <Link
              to="/"
              className="logo"
            >
              justjoin.it
            </Link>
          </div>
          <Route
            path="/:city?/:technology?"
          >
            {({ match }) => {
              let offers = this.state.offers;
              let offer;
              if (match.params.city !== 'offers') {
                if (match.params.city && match.params.city !== 'all') {
                  offers = offers.filter(offer => {
                    return offer.city.toLowerCase() === match.params.city;
                  })
                }
                if (match.params.technology) {
                  offers = offers.filter(offer => {
                    return offer.marker_icon === match.params.technology;
                  })
                }
              } else {
                offer = this.state.offers.find((offer) => offer.id === match.params.technology);
              }

              return (
                <React.Fragment>
                  <div className="filters">
                    <ul className="place">
                      <li>
                        <Link
                          to="/"
                          className={(!match.params.city || match.params.city === 'all' ? 'active' : '')}
                        >
                          All
                </Link>
                      </li>
                      {this.state.cities.map(city => (
                        <li
                          key={city}
                        >
                          <NavLink
                            to={`/${city[0].toLowerCase()}`}
                          >
                            {city[0]}
                          </NavLink>
                        </li>
                      ))}
                    </ul>
                    <ul className="technology">
                      <li>
                        <Link
                          to={`/${match.params.city || ''}`}
                          className={(!match.params.technology || match.params.technology === 'all' ? 'active' : '')}
                        >
                          All
                </Link>
                      </li>
                      {this.state.markers.map(marker => (
                        <li
                          key={marker[0]}
                        >
                          <NavLink
                            to={`/${match.params.city || 'all'}/${marker[0]}`}
                          >
                            <i className={`devicon-${marker[0]}-plain`} />
                          </NavLink>
                        </li>
                      ))}
                    </ul>
                  </div>
                  <div className="content">
                    <div className="column offers">
                      <Switch>
                        <Route
                          path="/offers/:id"
                          render={() => {
                            return (
                              <div>Offer Details</div>
                            );
                          }}
                        />
                        <Route
                          render={() => (
                            <ul>
                              {this.state.loading ?
                                new Array(5)
                                  .fill(0)
                                  .map((value, key) => ({
                                    id: key,
                                  })).map(this.renderOffer) :
                                offers
                                  .slice(0, 20)
                                  .map(this.renderOffer)}
                            </ul>
                          )}
                        />
                      </Switch>

                    </div>
                    <div className="column map">
                      <MapBox
                        offers={offers}
                        offerId={offer && offer.id}
                      />
                    </div>
                  </div>
                </React.Fragment>
              )
            }}
          </Route>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
