# JustJoin in React
To run project please

1. Clone repo
2. `yarn && yarn start`

Should work :)

# Mapbox implementation with many markers

There is few approaches wich can be used to achive our goal.

## Layers with features, without clustering

## react-mapbox-gl build in html markers with clustering
Death of perfomance

## Create markers with mapbox api and try to implement clustering with external lib
In this aproach we should get ability to render custom markers for clusters. Along with this we can use html to render them so it should be more adjustable.

## Layers with source with cluster set to true
Most advence version use custom predifined images and use many layers to display clusters, unclustered data etc witch layer filters (cool stuff)
https://www.mapbox.com/mapbox-gl-js/example/cluster/
We do not have ability to fully adjust how clusters will be rendered and we can use images or simple shapges (like circles).